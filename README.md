# LABASE-REF

># [Tutoriel en ligne](https://docs.google.com/document/d/1lY5HQPVNlhj-RlgvM38USWDaO07YdNN32tcF3S1vfHk/edit?usp=sharing)

># Description LABASE-REF (conférence physiques)

- la scène reférence contient tous les éléments dont le chemin est à mettre à jour après téléchargement
Profile de scène gère par défaut
- 3 sources vidéos (2 captures + 1 source NDI)
- 1 source media01 (vidéo de compagne LA BASE)
- habillage de base pour les conférence à La Base
- un lot de 8 synthés animées à paramétrer en manuel ou automatique (10s)
   - pour passer en manuel afficher ovl_low_manuel, cacher ovl_low_auto dans les scènes
   - pour passer en automatique 10s, afficher ovl_low_auto, cacher ovl_low_manuel
- raccourcis claviers synthés (lower third)
   - Ctrl+1 : intervenant 1
   - Ctrl+2 : intervenant 2
   - ...
   - Ctrl+8 : intervenant 8
- des raccourcis claviers par défaut pour les scènes
   - F1 : scn_video1
   - F2 : scn_video2
   - F3 : scn_video3
   - F4 : scn_media1
   - F12 : scn_multi1 avec animations
      - Alt+& : scn_multi1 > video1 plein écran
      - Alt+é : scn_multi1 > vidéo1 réduite + video2
      - Alt+" : scn_multi1 > vidéo1 réduite + video3
      - Alt+' : scn_multi1 > vidéo1 réduite + media1
	  
># Description LABASE-ZOOMHD (conférence zoom en ligne)

OBS Version : 26.0.2
Zoom Version : 5.3.1
Version de paramétrage : LABAREF-ZOOMHD-20201115

Le mode écran partagé n'est pour le moment pas encore optimum est se résume à afficher la fenêtre Zoom complète en mode "Cote à Cote" avec le document d'un côté et les vignette des intervenants de lautre avec un crop de la zone utile. Ce mode a l'avantage de laisser le contrôle de l'écran partage à l'intervenant et il a donc un un contrôle et un retour direct
Pour la réalisation, l'inconvenient c'est que ce n'est pas forcément travaillé, limité au design zoom et la disposition (ratio taille document partaéé/vignette intervenant) est à voir un peu en live lorsque l'intervenant partage son écran
De plus, dans ce mode on voit toutes les caméras, il faut donc s'assurer qu'il n'y a pas de caméra parasite comme le retour vidéo de la régie, la personne de la présentation de la base, l'animat.eur.rice ....
Mais il a l'avantage d'être une manière très simple de gérér un partage de document pour la réalisation
configuration prépropgrammée pour 6 intervenants max, au délà il faut passer en mode capture simple de la fenêtre zoom et éventuellement afficher les noms des intervenants

Rejoindre la réunion Zoom et atteindre que quelques participants se connectent avec vidéo pour faire les premiers réglages
Configuration de Zoom
Aller dans les propriétés vidéo
- Dans l'onglet "Vidéo"
   - Cocher "Ratio d'origine"
   - Cocher "HD"
   - Décocher "Toujours afficher les noms des participants sur leur vidéo
   - Cocher "masquer les participants sans vidéo" (sauf si Plus de 6 intervenants)

Gestion de l'écran partagé
- Dans l'onglet Général
   - Décocher "Utiliser deux moniteurs" => à tester si ce n'est pas une bonne solution par la suite
- Dans l'onglet Ecran partagé (à consolider, notamment pour le mode deux-fenêtre)
   - Cocher Ajuster le contenu partagé à la fenêtre Zoom
   - Cocher le mode "Côte à Côte"
   - Cocher "couper les notifications système lors du partage de bureau
Récupération de la source Zoom
Dans la scène "src_zoom",
- configurer l'élément de capture de fenêtre "dev_capture_zoom"
- Fenêtre : choisir [Zoom.exe]: Zoom Réunion
- Méthode de capture : Windows Graphics Capture (Windows 10 et +) ce n'est pas le choix automatique fait par OBS
- valider
- afficher l'élément "col_arriere_plan". Cet élément cache les autres images qui sont utilisées pour la mise au point
- sélectionner la scène scn_zoom pour vérifier la capture de fenêtre zoom


Adaptation de la conf au nombre de vidéo affichées dans Zoom
La configuration et les scènes savant s'adapter plus ou moins uatomatiquement au nombre de vignettes intervenants affichées dans le fenêtre zoom.
Cependant il faut explicitement lui préciser le nombre de vignette en cours d'affichage (qu'elles soit utiles ou pas) via les raccourcis CTRL+SHIT+<nombre>
- sélectionner la scène scn_zoom
- appuyer sur
   - CTRL+SHIFT+& pour passer en mode 1 intervenants
   - CTRL+SHIFT+é pour passer en mode 2 intervenants
   - CTRL+SHIFT+" pour passer en mode 3 intervenants
   - CTRL+SHIFT+' pour passer en mode 4 intervenants
   - CTRL+SHIFT+( pour passer en mode 5 intervenants
   - CTRL+SHIFT+- pour passer en mode 6 intervenants
   - Enfin CTRL+SHIFT+² pour passer en mode Zoom plein fenêtre afin d'afficher tout le monde, ainsi que pour gérer le partage d'écran de l'intervenant.
Ce mode permet aussi de récuéprer une image standard zoom lorsque le nombre d'intervenant change inopinément (déconnexion ...)
- SI tout va bien vous avez pu voir défiler toutes les conf jusqu'à 6 intervenants


- Pour chaque mode, le modèle de raccourcis est le même
   - F1 à F6 pour les gros plans de chaque intervenant
   - F9 pour un mode vignette intervenant + media 01
   - F10 pour le media 01 en gros plan
   - F12 pour le mode vignette intervenant uniquement

- Gestion des synthés
Les synthés des différents intéervenants sont gérés dans les scene LOW01 à LOW06 comme pour les confs physiques
Par contre ces scènes sont attachées à des intervenants fixes, scènes scn_intervenant_01 à 06
   - Procédure de mise à jour des synthés
      - Mettre à jour les scène LOW01 à LOW06
      - Dans chaque scène scn_intervenant_01 à 06, il y a un groupe grp_synthés_intervenants_XX qui contient toutes les synthés LOWXX
      - Choisir la synthés adaptée à l'intervenant
      - afficher/cacher les synthés intervenants grâce au raccourcis CTRL+Tabulation