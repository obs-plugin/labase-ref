$DIR_CURRENT = $(Get-Location).Path
Write-Host  'Current Directory : '$DIR_CURRENT
$Utf8NoBomEncoding = New-Object System.Text.UTF8Encoding $False
Get-ChildItem $DIR_CURRENT -Filter "LABASEREF.json" | ForEach-Object {
    $FILE_CURRENT = $_.FullName
    $FILE_CURRENT_NAME = $_.BaseName+$_.Extension
	$FILE_OUTPUT_DATA = [string]::Empty
	$DESTINATION_FILE = $_.BaseName + '_' +(Get-Date -Format "yyMMdd_HHmm")+ $_.Extension
	$PROFILE = $_.BaseName+'_'+(Get-Date -Format "yyMMdd_HHmm")
    Write-Host 'Current File : '$FILE_CURRENT' Desctination : '$DESTINATION_FILE
    #Out-File -FilePath ($DESTINATION_FILE) -Encoding utf8
    foreach ($line in Get-Content -Encoding utf8 -Raw $FILE_CURRENT){
        if($line -match 'LABASE-REF'){
            #Write-Host '!!!! replace !!!!: '+$line
            $line = ($line -replace "[^""]*LABASE-REF[^\\/]*","$DIR_CURRENT")
            #Write-Host $line
        }
        if($line -match '^    "name":.*"'){
            Write-Host '#### NAME :'$line
			$line = '    "name": "'+$PROFILE+'",'
        }
		$FILE_OUTPUT_DATA += $line.replace("\","/")
        #Write-Host 'OUTPUT:'$DIR_CURRENT+'\'$FILE_CURRENT '_out.log'
        #$line | Out-File -FilePath ($DESTINATION_FILE) -Append -Encoding utf8
    }
	[System.IO.File]::WriteAllLines($DESTINATION_FILE, $FILE_OUTPUT_DATA, $Utf8NoBomEncoding)
	if(!(Test-Path -Path "$DIR_CURRENT/archives" )){
		New-Item -Path $DIR_CURRENT -Name "archives" -ItemType "directory"
	}
	Move-Item -Path $FILE_CURRENT -Destination "$DIR_CURRENT/archives"
	Rename-Item -Path  "$DIR_CURRENT/archives/$FILE_CURRENT_NAME" -NewName "$DIR_CURRENT/archives/$DESTINATION_FILE"
	Rename-Item -Path  "$DIR_CURRENT/$DESTINATION_FILE" -NewName $FILE_CURRENT
}

Start-Sleep -Seconds 10